# My Unity Layouts

Clone this repo to the location Unity uses as local storage on your system. [This Stack Exchange thread](https://gamedev.stackexchange.com/questions/103025/where-does-unity-save-the-editor-window-layouts/103195) is correct at the time of writing.

Versions above 2018.4 store their layouts in the [../default](/default/) folder, which requires that the layouts should be provided twice.
